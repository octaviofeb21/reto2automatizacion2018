package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {
	
	WebDriver driver;
	
	public HomePage(WebDriver driver) {
		this.driver = driver;
	}

	public void seccionVuelos() {
		driver.findElement(By.cssSelector("li.nevo-header-navigation-menu-item.nevo-js-header-menu-item.FLIGHTS")).click();
	}
}
