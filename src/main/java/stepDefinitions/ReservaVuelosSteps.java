package stepDefinitions;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.HomePage;

public class ReservaVuelosSteps {
	WebDriver driver;
	HomePage homePage;
	
	@Before
	public void setUp(){
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
		this.driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		homePage = new HomePage(driver);
	}

	@After
	public void tearDown(){
		driver.quit();
	}
	
	@Given("el usuario entra a la pagina de despegar.com")
	public void ingresarPaginaDespegar() {
		driver.get("https://www.despegar.com.co/");
	}
  	
	@When("va a la pestana de vuelos")
	public void ingresarPestanaVuelos() {
		String cssPopup = "span.as-login-close.as-login-icon-close-circled";
		WebElement popUp = driver.findElement(By.cssSelector(cssPopup));
		
		if(popUp != null) {
			driver.findElement(By.cssSelector(cssPopup)).click();
		}
		
		homePage.seccionVuelos();
	}
	
  	@And("ingresa el lugar de origen para la salida del vuelo")
  	public void ingresarLugarOrigenVuelo() {
  		
  	}
  	
  	@And("ingresa el lugar de destino hacia donde va el vuelo")
  	public void ingresarDestinoVuelo() {
  		
  	}
  	
  	@And("indica su fecha de salida del vuelo")
  	public void fechaSalidaVuelo() {
  		
  	}
  	
  	@And("ingresa su fecha de regreso del vuelo")
  	public void fechaRegresoVuelo() {
  		
  	}
  	
  	@And("da clic en buscar vuelo")
  	public void clicBuscarVuelo() {
  		
  	}
  	
  	@And("busca las diez opciones mas favorables")
  	public void buscarPreciosVuelos() {
  		
  	}
  	
  	@Then("el usuario esta en la busqueda de vuelos y encontro los vuelos mas favorables")
  	public void estoyPaginaBusquedaVuelos() {
  		
  	}
	
  	
//  	@And("ingresa la fecha de llegada")
//  	public void ingresarFechaLlegada() {
//  		hotelPage.fechaLlegada("09/08/2018");
//	}
//  	
//  	@And("indica su fecha de salida")
//  	public void ingresarFechaSalida() {
//  		hotelPage.fechaSalida("09/15/2018");
//	}
//  	
//  	@And("busca un hotel en ese rango de fechas")
//  	public void clicEnBuscar() {
//  		hotelPage.clicBuscar();
//	}
//  	
//  	@And("selecciona la cantidad de estrellas del hotel")
//  	public void seleccionarEstrellasHotel(){
//  		busquedaHotelesPage.escogerEstrellasHotel();
//	}
//  	
//  	@And("selecciona un hotel")
//  	public void seleccionarHotel() throws InterruptedException {
//  		busquedaHotelesPage.escogerHotel();
//	}
//  	
//  	@And("el usuario da clic en reservar hotel")
//  	public void clicReservarHotel() {
//  		// Cambio de pagina
//		ArrayList<String> windows = new ArrayList<String>(driver.getWindowHandles());
//		driver.switchTo().window(windows.get(1));
//		reservaPage.reservarHotel();
//	}
//  	
//  	@And("luego da clic en pagar ahora")
//  	public void pagarAhora() {
//		reservaPage.clicPagarAhora();
//  	}
//  	
//  	@Then("el usuario queda en la pagina de pago")
//  	public void validarPaginaPago() {
//  	  	String paginaEsperada = pagoPage.estoyEnPaginaPago();
//  	  	String paginaEnLaCualEstoy = driver.getTitle();
//		Assert.assertEquals(paginaEsperada, paginaEnLaCualEstoy);
//  	}
}
