Feature: busqueda de tiquetes en la pagina despegar.com
	Yo como viajero quiero buscar los tiquites m�s baratos de Medellin a Cartagena.

  Scenario: busqueda exitosa de vuelos
  	Given el usuario entra a la pagina de despegar.com
  	When va a la pestana de vuelos
  	And ingresa el lugar de origen para la salida del vuelo
  	And ingresa el lugar de destino hacia donde va el vuelo
  	And indica su fecha de salida del vuelo
  	And ingresa su fecha de regreso del vuelo
  	And da clic en buscar vuelo
  	And busca las diez opciones mas favorables
  	Then el usuario esta en la busqueda de vuelos y encontro los vuelos mas favorables